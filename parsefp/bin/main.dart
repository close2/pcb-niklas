import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:parsefp/template.dart';

main(List<String> arguments) {
  var f = new File(arguments[0]);
  var fpPoints = f.readAsStringSync();
  var lines = new LineSplitter().convert(fpPoints);

  // 	ElementLine(13665 0 13587 9 8)
  var r = new RegExp(r'.*ElementLine\(([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+).*');
  var points = <Point>[];

  print(before);
  for (var l in lines) {
    var match = r.firstMatch(l);
    if (match == null) {
      continue;
    }
    var from = new Point(int.parse(match[1]), int.parse(match[2]));
    var to = new Point(int.parse(match[3]), int.parse(match[4]));

    if (from != to || from != points.first) {
      points.add(from);
    } else {
      var kicadPts = points.map((p) => '(xy ${p.x / 100} ${p.y / 100})').join(' ');
      print(repeatBefore);
      print(kicadPts);
      print(repeatAfter);
      points = <Point>[];
    }
  }
  print(after);
}
